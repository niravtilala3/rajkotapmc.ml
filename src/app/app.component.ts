import {MediaMatcher} from '@angular/cdk/layout';
import {ChangeDetectorRef, Component, OnDestroy, OnInit} from '@angular/core';
import { CropService } from './crop.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit, OnDestroy {

  constructor(private cropService: CropService, changeDetectorRef: ChangeDetectorRef, media: MediaMatcher) {
    this.mobileQuery = media.matchMedia('(max-width: 600px)');
    this._mobileQueryListener = () => changeDetectorRef.detectChanges();
    this.mobileQuery.addListener(this._mobileQueryListener);
  }
  title = 'rajkotapmc';
  mobileQuery: MediaQueryList;
  // fillerNav = Array.from({length: 10}, (_, i) => `Nav Item ${i + 1}`);
  toDayCropList;
  toDay;
  isVisible = false;
  // tslint:disable-next-line:variable-name
  private _mobileQueryListener: () => void;

  ngOnInit(): void {
    this.cropService.getTodayCrop().then( (res) => {
      this.toDayCropList = res[0].corpList;
      if(this.toDayCropList === []){
        this.isVisible = true;
      }
      this.toDay = res[0].updated;
      // console.log(this.toDayCropList);
    });
  }

  ngOnDestroy(): void {
    this.mobileQuery.removeListener(this._mobileQueryListener);
  }

}
