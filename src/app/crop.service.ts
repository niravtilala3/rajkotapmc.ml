import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';

const httpOptions = {
  headers: new HttpHeaders({
            'Access-Control-Allow-Origin': '*',
            'Access-Control-Allow-Methods': 'GET, POST',
            'Content-Type': 'application/json'
  })
};
@Injectable()
export class CropService {
  private cropUrl = 'https://rajkotagriculture.herokuapp.com/api/get';
  constructor(private http: HttpClient) {
  }
  getTodayCrop() {
    return this.http.get(this.cropUrl, httpOptions).toPromise();
  }
}
